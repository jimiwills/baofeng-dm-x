#!/usr/bin/perl

use strict;
use warnings;

$limit = 200;


my $file;
# open(my $fh, '<', '93.data') or die $!;
open(my $fh, '<', 'Jimi20200122a.data') or die $!;
binmode($fh);
print read($fh,$file,245760);
close($fh);
print "\n";

# foreach (1..18){
# 	print "$_ ".index($file, "Channel $_")."\n";
# }

my $hex = unpack("H*", $file);


# foreach (1..18){
# 	$_ = "0$_" if $_ < 10;
# 	my $where = index($hex, $_."000014")/2;
# 	print "$_ $where\n";
# }

# exit;


print length($file)."\n";
print ((length($file)*2)."\n");

print length($hex)."\n";

open(my $fo, '>', 'out.data') or die $!;
binmode($fo);
print $fo $file;

open(my $ch, "<", "channels.txt") or die $!;
my @ch = <$ch>;
close($ch);

shift @ch; # header line

seek($fo, 16384, 0) or die $!;
my $count = 0;
foreach(@ch){
	s/[\n\r]//g;
	my ($num,$nam,$ten,$len,$o10,$rx,$tx,$mode,$pwr,
		$rxt,$txt,$cc,$grp,$ctc,$slot,$bw,$sq)
	 = split /\t/;
	while(length($ten)<11){
		$ten .= chr(0);
	}
	print $fo $ten;
	$count ++;
	last if $count >= $limit;
}
seek($fo, 0x3000, 0) or die $!;
print $fo pack("S",$count);



sub hexifynumber {
	my ($number,$beforepoint,$afterpoint) = @_;

	$number .= '.' unless $number =~ /\./;
	my ($b,$a) = split(/\./, $number);
	$a.="0" while length($a) < $afterpoint;
	$b="0$b" while length($b) < $beforepoint;
	die "$number doesn't fit into {$beforepoint}.{$afterpoint}"
		if length($a) > $afterpoint || length($b) > $beforepoint;
	return join '', reverse split /(..)/, $b.$a;
}


seek($fo, 12304, 0) or die $!;
$count = 0;
foreach(@ch){
	s/[\n\r]//g;
	my ($num,$nam,$ten,$len,$o10,$rx,$tx,$mode,$pwr,
		$rxt,$txt,$cc,$grp,$ctc,$slot,$bw,$sq)
	 	= split /\t/;

	my $rxtx = hexifynumber($rx, 3, 5) 
		. hexifynumber($tx, 3, 5);

	my $tonehex = ($rxt  =~ /\d/ ? hexifynumber($rxt, 3, 1) : "ffff")
				. ($txt  =~ /\d/ ? hexifynumber($txt, 3, 1) : "ffff");


	my $mdhex = $mode eq 'Analog' 
		? "020000000000" : "420000000000";

	my $slothex = $slot == 1 ? "0" : "1";
	my $cchex = $cc."00";
	my $grphex = "000".$grp."01";
	my $therest = "0000004000000000000a0a5100000000000000000000000000";

	my $hexrec = "$rxtx$mdhex$slothex$cchex$grphex$tonehex$therest";

	my $record = pack("H*",$hexrec);
	die "record length: ".length($record) unless length($record) == 48;

	print $fo $record;
	$count ++;
	if($count == 85){
		seek($fo, 61488, 0);
	}
	last if $count >= $limit;
}

close($fo);

__END__


while($hex =~ /\G.*?(3134352e.{12}00)/g) {
	my $byte0 = $-[1] / 2;
	my $byteN = $+[1] / 2;
	print "$byte0 $byteN: $1\n";
	# 16384 16395 is ch 1
	# 16681 16692 is ch 28
	# they are 11 bytes each
}
print( ((16692-16384)/28)."\n");

while($hex =~ /\G.*?(00\d\d\d\d14)\1(.{80})/g) {
	my $byte0 = $-[1] / 2;
	my $byteN = $+[2] / 2;
	print "$byte0 $byteN: $1 $2\n";
	# 12304 12352 is ch 1
	# 13600 13648 is ch 28
	# they are 48 bytes each
}

print( ((13648-12304)/28)."\n");





1A vs 1C at 3000
likely number of records... I have 28 now...
28 in hex: 1c
Nice!

Seems like:
34F0 to 3520 are a record...

This record  	Previous
(digital)		(analog)
00 25 55 14 - 00 00 55 14
00 25 55 14 - 00 00 55 14
02 00 00 00 - 42 00 00 00
00 00 00 00 - 00 00 00 00
00 00 01 FF - 00 01 01 FF
FF FF FF 00 - FF FF FF 00
00 00 40 00 - 00 00 40 00
00 00 00 00 - 00 00 00 00
0A 0A 51 00 - 0A 0A 51 00
00 00 00 00 - 00 00 00 00
00 00 00 00 - 00 00 00 00
00 00 00 00 - 00 00 00 00

145.525
/10 int % 100 = 14
*10 int % 100 = 55
*1000 int % 100 = 25
*100000 int % 100 = 00

@rx = map {int($_) % 100} ($rx*100000, $rx*1000, $rx*10, $rx/10)


Then the names...
Ch27, 28 are in 
411e .. 4128 and
412a .. 4233
separated by 00s

The actual files are both 240kb

Then some ff ff ff ff at 0001:E034 
has become 00 00 00 01  presumable analog, then digital?

And that's all the differences... so let's try adding a pair...


__DATA__