#!/usr/bin/perl

# from-template.pl



my $file;
# open(my $fh, '<', '93.data') or die $!;
open(my $fh, '<', 'template.data') or die $!;
binmode($fh);
print read($fh,$file,245760);
close($fh);
print "\n";

#my $hex = unpack("H*", $file);

my $blank = pack("H*","0000004000000040");

my $blanks = -1;
my $i = 0;
while($i >= 0){
	$i++;
	$i = index($file,$blank,$i);
	$blanks++;
}

print "$blanks\n";

open(my $ch, "<", "channels.txt") or die $!;
my @ch = <$ch>;
close($ch);

shift @ch; # header line

my $limit = 997;

# 46, 47 are in 1E05b and 1E05d
# if 46 is starting 1e05a and each are two bytes...
# they start at 1E000


my $count = 0;
foreach(@ch){
	s/[\n\r]//g;
	my ($num,$nam,$ten,$len,$o10,$rx,$tx,$mode,$pwr,
		$rxt,$txt,$cc,$grp,$ctc,$slot,$bw,$sq)
	 = split /\t/;

	my $rxtx = hexifynumber($rx, 3, 5) 
		. hexifynumber($tx, 3, 5);

	my $tonehex = ($rxt  =~ /\d/ ? hexifynumber($rxt, 3, 1) : "ffff")
				. ($txt  =~ /\d/ ? hexifynumber($txt, 3, 1) : "ffff");


	my $mdhex = $mode eq 'Analog' 
		? "020000000000" : "420000000000";

	$grp = 0 if $cc == 0;
	$ctc = 0 if $cc == 0;

	my $slothex = $slot == 1 ? "0" : "1";
	my $cchex = $cc."00";
	my $grphex = "000".$grp."01";
	my $therest = "0000004000000000000a0a5100000000000000000000000000";

	my $hexrec = "$rxtx$mdhex$slothex$cchex$grphex$tonehex$therest";

	my $record = pack("H*",$hexrec);

	die "record length: ".length($record) 
		unless length($record) == 48;


	my $i = index($file,$blank);
	die if $i < 0;
	substr($file, $i, 48, $record);

	if($ctc){
		substr($file, 0x1E000 + $count *2 + 1, 1, chr($ctc))
	}

	$count ++;
	while(length($ten)<11){
		$ten .= chr(0);
	}
	die "$ten" unless length($ten) == 11;

	$i = index($file,"Channel $count");
	#print "$i\n";
	die if $i < 0;
	substr($file, $i, 11, $ten);

	last if $count >= $limit;
}

open(my $fo, '>', 'out.data') or die $!;
binmode($fo);
print $fo $file;
close($fo);

sub hexifynumber {
	my ($number,$beforepoint,$afterpoint) = @_;

	$number .= '.' unless $number =~ /\./;
	my ($b,$a) = split(/\./, $number);
	$a.="0" while length($a) < $afterpoint;
	$b="0$b" while length($b) < $beforepoint;
	die "$number doesn't fit into {$beforepoint}.{$afterpoint}"
		if length($a) > $afterpoint || length($b) > $beforepoint;
	return join '', reverse split /(..)/, $b.$a;
}
